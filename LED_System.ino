#include "settings.h"
#include <IRremote.h>
#include "LED_Handler.h"
#include <EEPROM.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

//<--------------------------------- OBJECTS ---------------------------------------
IRrecv irrecv(IR_PIN);
decode_results signals;
LED_Handler led_handler;

//<--------------------------------- STATES
LED_COLOR led_color = RED;
LED_STATE led_state = SOLID_COLOR;
SYSTEM_STATE system_state = STILL_ON;

//<--------------------------------- FLAGS -----------------------------------------
bool movementDetected_pir_1 = false;
bool movementDetected_pir_2 = false;
bool changeBrightness_flag = false;
bool changeColor_flag = false;
bool changeLEDState_flag = false;
bool changeSystemState_flag = false;
bool rainbow_flag = false;
bool rainbow_timer = false;
//need to change led after RAINBOW
int currentSystemState;

//<--------------------------------- FUNCTIONS -------------------------------------
void IR_interrupt();
void readEEPROM();
void saveBrightness(int brightness);
void saveLEDColor(int color);
void saveLEDState(int state);
void saveSystemState(int state);
void changeLEDColor(int newColor);
void changeLEDState(int newState);
void changeSystemState(int newState);
bool photoresitorData();
void PIR1_Handler(); // down
void PIR2_Handler(); // up

//<--------------------------------- VARIABLES -------------------------------------
int timer;
int lightning_timer;
int rainbow_interval = 800;

//<--------------------------------- SETUP FUNCTION --------------------------------
void setup()
{

  //saveLEDColor(led_color);
  //saveLEDState(SOLID_COLOR);
  //saveSystemState(STILL_ON);
  Serial.begin(9600);
  pinMode(PIR1_SENSOR_PIN, INPUT);
  pinMode(PIR2_SENSOR_PIN, INPUT);
  irrecv.enableIRIn();
  // interrupts
  attachInterrupt(digitalPinToInterrupt(IR_PIN), IR_interrupt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIR1_SENSOR_PIN),PIR1_Handler, RISING);
  attachInterrupt(digitalPinToInterrupt(PIR2_SENSOR_PIN),PIR2_Handler, RISING);
  // load saved settings
  //readEEPROM();
  LED_Initialization(led_handler.getBrightness());

 //<---------------- STARTUP ACTIONS 
  startupAnim(1);
  ResetLED();

  if(led_state == SOLID_COLOR && system_state == STILL_ON)
  {
      colorWipe(led_color);
  }
  if (led_state == RAINBOW)
  {
    rainbow_flag = true;
  }

  timer = 0;
  lightning_timer = 0;
}

//<--------------------------------- MAIN LOOP -------------------------------------
void loop() {
  
  //change brightness
  if(changeBrightness_flag)
  {
    updateLED_Brightness(led_handler.getBrightness());
    changeBrightness_flag = false;
  }

  //<----------------------- system_state == STILL_ON
    if(system_state == STILL_ON && led_state == SOLID_COLOR && changeColor_flag == true)
    { 
        colorWipe(led_color);
        changeColor_flag = false;
    }
   if (system_state == STILL_ON && led_state == RAINBOW && rainbow_flag == true)
    { 
      if(rainbow_timer == true)
      {
        rainbowFade2White(2);
        rainbow_timer = false;
      }
      if(rainbow_timer == false)
      {
        timer++;
        if(timer >= rainbow_interval)
        {
          timer = 0;
          rainbow_timer = true;
        }
      }
    }
 //<------------------------- system_state == SENSOR_CONTROL
    if(system_state == SENSOR_CONTROL && photoresitorData() == true && led_state == SOLID_COLOR)
    {
      //from down to up
      if(movementDetected_pir_1 == true)
      {
          Serial.println("COLOR WIPE -> SENSOR CONTROL FROM DOWN TO UP");
          updateLED_Brightness(led_handler.getBrightness());
          colorWipe(led_color);
          delay(LIGHTNING_TIME);
          Serial.println("INVERSE COLOR WIPE -> SENSOR CONTROL");
          inverseColorWipe(1);
          movementDetected_pir_1 = false;
      }
      //from up to down
      if(movementDetected_pir_2 == true)
      {
          Serial.println("COLOR WIPE -> SENSOR CONTROL FROM UP TO DOWN");
          updateLED_Brightness(led_handler.getBrightness());
          colorWipeFromUpToDown(led_color);
          delay(LIGHTNING_TIME);
          Serial.println("INVERSE COLOR WIPE -> SENSOR CONTROL");
          inverseColorWipeFromUpToDown(1);
          movementDetected_pir_2 = false;
      }
    }
    //RAINBOW
    if(system_state == SENSOR_CONTROL && photoresitorData() == true && led_state == RAINBOW)
    {
      if(movementDetected_pir_1 == true)
      {
        Serial.println("RAINBOW -> SENSOR CONTROL");
        updateLED_Brightness(led_handler.getBrightness());
        rainbowFade2White(6);
        Serial.println("INVERSE COLOR WIPE -> SENSOR CONTROL");
        inverseColorWipe(1);
        movementDetected_pir_1 = false;
      }
      //from up to down
     if(movementDetected_pir_2 == true)
      {
        Serial.println("RAINBOW -> SENSOR CONTROL");
        updateLED_Brightness(led_handler.getBrightness());
        rainbowFade2White(6);
        Serial.println("INVERSE COLOR WIPE -> SENSOR CONTROL");
        inverseColorWipeFromUpToDown(1);
        movementDetected_pir_2 = false;
      }
    }

  delay(10);
}

//<--------------------------------- FUNCTIONS DEFINITIONS -------------------------
 
void IR_interrupt()
{
  if (irrecv.decode(&signals)) {

    if (signals.value == 0xFFA25D) // 1
    {
      if(led_state == RAINBOW)
      {
        changeColor_flag = true;
      }
      changeLEDState(LED_STATE::SOLID_COLOR);
      saveLEDState(led_state);
      Serial.println("SOLID COLOR MODE");
    }
    else if (signals.value == 0xFF629D) // 2
    {
      changeLEDState(LED_STATE::WAVE);
      saveLEDState(led_state);
      Serial.println("WAVE MODE");
    }
    else if (signals.value == 0xFFE21D) // 3
    {
      changeLEDState(LED_STATE::RAINBOW);
      saveLEDState(led_state);
      rainbow_flag = true;
      rainbow_timer = true;
      Serial.println("RAINBOW MODE");
    }
    else if (signals.value == 0xFF22DD) // 4
    {

    }
    else if (signals.value == 0xFF02FD) // 5
    {

    }
    else if (signals.value == 0xFFC23D) // 6
    {

    }
    else if (signals.value == 0xFFE01F) // 7
    {

    }
    else if (signals.value == 0xFFA857) // 8
    {

    }
    else if (signals.value == 0xFF906F) //  9
    {

    }
    else if (signals.value == 0xFF9867) // 0
    {
      system_state = OFF;
      Serial.println("LEDS OFF");
        for(int i = 0; i <= 25; i++)
        {
          updateLED_Brightness(250-i*10);
          delay(20);
        }
    }
    else if (signals.value == 0xFF6897) // *
    {
      //enable leds
      system_state = STILL_ON;
      saveSystemState(system_state);
      changeColor_flag = true;
      updateLED_Brightness(led_handler.getBrightness());
      Serial.println("LEDS STILL_ON");
    }
    else if (signals.value == 0xFFB04F) // #
    {
      //LEDS OFF
      system_state = OFF;
      Serial.println("LEDS OFF");
        for(int i = 0; i <= 25; i++)
        {
          updateLED_Brightness(250-i*10);
          delay(20);
        }
    }
    else if (signals.value == 0xFF38C7) // OK
    {
        system_state = SENSOR_CONTROL;
        saveSystemState(system_state);
        Serial.println("SENSOR_CONTROL ON");
        for(int i = 0; i <= 25; i++)
        {
          updateLED_Brightness(250-i*10);
          delay(15);
        }
    }
    else if (signals.value == 0xFF18E7) // UP
    {
      led_handler.changeBrightness(led_handler.getBrightness() + 30);
      changeBrightness_flag = true;
      saveBrightness(led_handler.getBrightness());
      Serial.print("Brightness: "); Serial.println(led_handler.getBrightness());
    }
    else if (signals.value == 0xFF5AA5) // RIGHT
    {
      changeLEDColor(led_color + 1); 
      changeColor_flag = true;
      Serial.print("Color State: "); Serial.println(led_color);
    }
    else if (signals.value == 0xFF4AB5) // DOWN
    {
      led_handler.changeBrightness(led_handler.getBrightness() - 30);
      changeBrightness_flag = true;
      saveBrightness(led_handler.getBrightness());
      Serial.print("Brightness: "); Serial.println(led_handler.getBrightness());
    }
    else if (signals.value == 0xFF10EF) // LEFT
    {
      changeLEDColor(led_color - 1);
      changeColor_flag = true;
      Serial.print("Color State: "); Serial.println(led_color);
    }
    else
    {
    }
    irrecv.resume();
    changeSystemState_flag = true;
  }
}

void PIR1_Handler()
{
  if(movementDetected_pir_1 == false &&movementDetected_pir_2 == false  && photoresitorData() == true && system_state == SENSOR_CONTROL)
  {
    movementDetected_pir_1 = true;
    Serial.println("Registered movement PIR 1");
  }
}

void PIR2_Handler()
{
  if(movementDetected_pir_1 == false &&movementDetected_pir_2 == false  && photoresitorData() == true && system_state == SENSOR_CONTROL)
  {
    movementDetected_pir_2 = true;
    Serial.println("Registered movement PIR 2");
  }
}

void readEEPROM()
{
  led_handler.changeBrightness(EEPROM.read(0));
  led_color = EEPROM.read(1);
  led_state = EEPROM.read(2);
  system_state = EEPROM.read(3);

  Serial.print("LED brightness: "); Serial.println(led_handler.getBrightness());
  Serial.print("LED color: "); Serial.println(led_color);
  Serial.print("LED state: "); Serial.println(led_state);
  Serial.print("System state: "); Serial.println(system_state);
}

void saveBrightness(int brightness)
{
  EEPROM.write(0, brightness);
}

void saveLEDColor(int color)
{
  EEPROM.write(1, color);
}

void saveLEDState(int state)
{
  EEPROM.write(2,state);
}

void saveSystemState(int state)
{
  EEPROM.write(3,state);
}

void changeLEDColor(int newColor)
{
  if(newColor < 0)
  {
    newColor = 9;
  }
  else if (newColor > 9)
  {
    newColor = 0;
  }
  led_color = newColor;
  saveLEDColor(led_color);
}

void changeLEDState(int newState)
{
  if(newState < 0)
  {
    newState = 2;
  }
  else if (newState > 2)
  {
    newState = 0;
  }
  led_state = newState;
  saveLEDState(led_state);
}

void changeSystemState(int newState)
{
  if(newState < 0)
  {
    newState = 2;
  }
  else if (newState > 2)
  {
    newState = 0;
  }
  system_state = newState;

  saveSystemState(system_state);
}

bool photoresitorData()
{
  bool state;
  if(analogRead(PHOTORESISTOR_PIN) <= PHOTORESISTOR_IR_VAL)
  {
    state = true;
  }
  else
  {
    state = false;
  }
  
  return state;
}
