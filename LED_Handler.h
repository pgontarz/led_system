// LED class which control animations
#pragma once
#include "settings.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

//<------------------------------------ SETTINGS
class LED_Handler
{
  private:
    int brightness;
    int color; // colors are described in settings.h file
  public:
    LED_Handler(/* args */);
    ~LED_Handler();
    void changeBrightness(int newBrightness);
    int getBrightness();
};
//<------------------------------------ LED SYSTEM
void LED_Initialization(int brightness);
void updateLED_Brightness(int brightness);
void updateLED(int color);
void ResetLED();

//<------------------------------------ ANIMATIONS
void startupAnim(int wait);
void colorWipe(int color);
void colorWipeFromUpToDown(int color);
void wipe(uint32_t color, int wait);
void wipeFromUpToDown(uint32_t color, int wait);
void inverseColorWipe(int wait);
void inverseColorWipeFromUpToDown(int wait);
void pulseWhite(uint8_t wait);
void rainbowFade2White(int rainbowLoops);
void colorRED(int brightness);
