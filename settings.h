//System documentation and LED settings
#pragma once
#include <arduino.h>

/*EEPROM REGISTERS
0 - brightness
1 - LED_COLOR
2 - LED_STATE
3 - SYSTEM_STATE
*/

//<--------------------------------- PINS DECLARATION ----------------------------------
#define PHOTORESISTOR_PIN   A1
#define IR_PIN              18
#define PIR1_SENSOR_PIN      21
#define PIR2_SENSOR_PIN      19
//leds
#define LED_PIN_1     2
#define LED_PIN_2     3
#define LED_PIN_3     4
#define LED_PIN_4     5
#define LED_PIN_5     6
#define LED_PIN_6     7
#define LED_PIN_7     8
#define LED_PIN_8     9
#define LED_PIN_9     10
#define LED_PIN_10    11
#define LED_PIN_11    12
#define LED_PIN_12    13

#define LED_DELAY 2

//<--------------------------------- GLOBAL SETTINGS -----------------------------------
#define DEBUG false
#define PHOTORESISTOR_IR_VAL 105
//LEDS SETTINGS
#define LED_COUNT 49
#define LIGHTNING_TIME 20000

//<--------------------------------- ENUM TYPES ----------------------------------------
enum LED_COLOR{
    RED   = 0,
    ORANGE = 1,
    GOLD = 2,
    YELLOW = 3,
    GREEN = 4,
    LIGHT_BLUE = 5,
    BLUE = 6,
    NAVY_BLUE = 7,
    VIOLET = 8,
    WHITE = 9
};

enum LED_STATE{
    SOLID_COLOR = 0,
    WAVE        = 1,
    RAINBOW     = 2
};

enum SYSTEM_STATE{
    OFF            = 0,
    STILL_ON       = 1,
    SENSOR_CONTROL = 2
};
