#include "LED_Handler.h"


Adafruit_NeoPixel strip1(LED_COUNT, LED_PIN_1, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip2(LED_COUNT, LED_PIN_2, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip3(LED_COUNT, LED_PIN_3, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip4(LED_COUNT, LED_PIN_4, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip5(LED_COUNT, LED_PIN_5, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip6(LED_COUNT, LED_PIN_6, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip7(LED_COUNT, LED_PIN_7, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip8(LED_COUNT, LED_PIN_8, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip9(LED_COUNT, LED_PIN_9, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip10(LED_COUNT, LED_PIN_10, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip11(LED_COUNT, LED_PIN_11, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel strip12(LED_COUNT, LED_PIN_12, NEO_GRBW + NEO_KHZ800);

Adafruit_NeoPixel strip[] = {strip1,strip2,strip3,strip4,strip5,strip6,strip7,strip8,strip9,strip10,strip11,strip12};

//<------------------------------------ SETTINGS
LED_Handler::LED_Handler(/* args */)
{
    this->brightness = 250;
    this->color = LED_COLOR::RED;
}

LED_Handler::~LED_Handler()
{
}

void LED_Handler::changeBrightness(int newBrightness)
{
    if(newBrightness <= 0)
    {
        newBrightness = 10;
    }
    else if(newBrightness >= 255)
    {
        newBrightness = 255;
    }
    this->brightness = newBrightness;
}

int LED_Handler::getBrightness()
{
    return this->brightness;
}

//<------------------------------------ LED SYSTEM
void LED_Initialization(int brightness){

  for(int i = 0; i<12; i++)
  {
    strip[i].begin();
    strip[i].show();
    strip[i].setBrightness(brightness); 
  }
}

void updateLED_Brightness(int brightness)
{
  for(int i = 0; i<12; i++)
  {    
    strip[i].setBrightness(brightness); 
    strip[i].show();
  }
}

void updateLED()
{
   for(int i = 0; i<12; i++)
  {    
    strip[i].show();
  }
}

void ResetLED()
{
  for(int i = 0; i<12; i++)
  {    
    strip[i].fill(strip[i].Color(0, 0, 0));
    strip[i].show();
  }
}

void colorDecoder(int color)
{
    
}

//<------------------------------------ ANIMATIONS
void colorRED(int brightness)
{

}

void colorWipe(int color) {

  switch (color)
  {
    case 0:
      wipe(strip[0].Color(255,   0,   0),LED_DELAY); //red
      break;

    case 1: 
      wipe(strip[0].Color(  255, 30,   30),LED_DELAY); //orange
      break;

    case 2:
      wipe(strip[0].Color(  255, 160,   0),LED_DELAY); //gold
      break;

    case 3:
      wipe(strip[0].Color(  255,   255,  29),LED_DELAY); //yellow
      break;

    case 4:
      wipe(strip[0].Color(  0,   255, 0),LED_DELAY);//green
      break;

    case 5:
        wipe(strip[0].Color(  51,   255, 50),LED_DELAY); // light blue
      break;

    case 6:
      wipe(strip[0].Color(  0,   0, 255),LED_DELAY); // blue
      break;

    case 7:
      wipe(strip[0].Color(  0,   51,   102),LED_DELAY); //navi blue
      break;

    case 8:
      wipe(strip[0].Color(  204,   0,   204),LED_DELAY); // violet
      break;

    case 9:
      wipe(strip[0].Color(  0,   0,   0, 255),LED_DELAY); // natural white
      break;

    default:
      Serial.println("Color error.");
      break;
  }
}

void colorWipeFromUpToDown(int color) {

  switch (color)
  {
    case 0:
      wipeFromUpToDown(strip[0].Color(255,   0,   0),LED_DELAY); //red
      break;

    case 1: 
      wipeFromUpToDown(strip[0].Color(  255, 30,   30),LED_DELAY); //orange
      break;

    case 2:
      wipeFromUpToDown(strip[0].Color(  255, 160,   0),LED_DELAY); //gold
      break;

    case 3:
      wipeFromUpToDown(strip[0].Color(  255,   255,  29),LED_DELAY); //yellow
      break;

    case 4:
      wipeFromUpToDown(strip[0].Color(  0,   255, 0),LED_DELAY);//green
      break;

    case 5:
      wipeFromUpToDown(strip[0].Color(  51,   255, 50),LED_DELAY); // light blue
      break;

    case 6:
      wipeFromUpToDown(strip[0].Color(  0,   0, 255),LED_DELAY); // blue
      break;

    case 7:
      wipeFromUpToDown(strip[0].Color(  0,   51,   102),LED_DELAY); //navi blue
      break;

    case 8:
      wipeFromUpToDown(strip[0].Color(  204,   0,   204),LED_DELAY); // violet
      break;

    case 9:
      wipeFromUpToDown(strip[0].Color(  0,   0,   0, 255),LED_DELAY); // natural white
      break;

    default:
      Serial.println("Color error.");
      break;
  }
}

void wipe(uint32_t color, int wait) {

    for(int j = 0; j<12; j++)
    {
      if(j%2==0)
      {
        for(int i=0; i<strip[j].numPixels(); i++) 
        { 
          strip[j].setPixelColor(i, color);         
          strip[j].show(); 
          delay(wait);                        
        }  
      }
      else
      {
        for(int i=0; i<strip[j].numPixels(); i++) 
        { 
          strip[j].setPixelColor(LED_COUNT - i - 1, color);         
          strip[j].show(); 
          delay(wait);                        
        }
      }      
    }
}

void wipeFromUpToDown(uint32_t color, int wait) {

    for(int j = 0; j<12; j++)
    {
      if(j%2==0)
      {
        for(int i=0; i<strip[11-j].numPixels(); i++) 
        { 
          strip[11-j].setPixelColor(i, color);         
          strip[11-j].show(); 
          delay(wait);                        
        }  
      }
      else
      {
        for(int i=0; i<strip[11-j].numPixels(); i++) 
        { 
          strip[11-j].setPixelColor(LED_COUNT - i - 1, color);         
          strip[11-j].show(); 
          delay(wait);                        
        }
      }      
    }
}

void startupAnim(int wait)
{
  wipe(strip[0].Color(  0,   0,   0, 255),wait); // natural white
}

void inverseColorWipe(int wait)
{
  //turn LED off [sensor control]
    for(int j = 0; j<12; j++)
    {
        for(int i=0; i<strip[11-j].numPixels(); i++) 
        { 
          if(j%2==1)
          {
            strip[11-j].setPixelColor(i, strip[11-j].Color(0,   0,   0));         
            strip[11-j].show(); 
            delay(wait); 
          }
          else if(j%2==0)
          {
            strip[11-j].setPixelColor(LED_COUNT - i - 1, strip[11-j].Color(0,   0,   0));         
            strip[11-j].show(); 
            delay(wait);
          }     
          else
          {
                              
          }
        }         
    }
}

void inverseColorWipeFromUpToDown(int wait)
{
  //turn LED off [sensor control]
    for(int j = 0; j<12; j++)
    {
        for(int i=0; i<strip[j].numPixels(); i++) 
        { 
          if(j%2==1)
          {
            strip[j].setPixelColor(i, strip[j].Color(0,   0,   0));         
            strip[j].show(); 
            delay(wait); 
          }
          else if(j%2==0)
          {
            strip[j].setPixelColor(LED_COUNT - i - 1, strip[j].Color(0,   0,   0));         
            strip[j].show(); 
            delay(wait);
          }     
          else
          {
                              
          }
        }         
    }
}

void pulseWhite(uint8_t wait) {
  /*
  for(int j=0; j<256; j++) { // Ramp up from 0 to 255
    // Fill entire strip with white at gamma-corrected brightness level 'j':
    strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
    strip.show();
    delay(wait);
  }

  for(int j=255; j>=0; j--) { // Ramp down from 255 to 0
    strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
    strip.show();
    delay(wait);
  }
  */
}

void rainbowFade2White(int rainbowLoops) {
  int fadeVal=100, fadeMax=100;

  
  for(uint32_t firstPixelHue = 0; firstPixelHue < rainbowLoops*65536;
    firstPixelHue += 256*3) {

  for(int k = 0; k<12; k++)
  {
    for(int i=0; i<strip[k].numPixels(); i++) { // For each pixel in strip...

      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip[k].numPixels());

      strip[k].setPixelColor(i, strip[k].gamma32(strip[k].ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }
    strip[k].show();
  }
  /*

    if(firstPixelHue < 65536) {                              
      if(fadeVal < fadeMax) fadeVal++;                      
    } else if(firstPixelHue >= ((rainbowLoops-1) * 65536)) {
      if(fadeVal > 65) fadeVal--;                             
    } else {
      fadeVal = fadeMax; 
    }
    */
  }
}
